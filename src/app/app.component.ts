import { Component } from "@angular/core";
import { createWorker } from "tesseract.js";
import {
  FormGroup,
  FormControlName,
  FormControl,
  FormBuilder,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "MazzSolutuons OCR Solutions";
  selectedImage;
  imageUrl;
  updateprofileimg: FormGroup;
  img_upload;
  url = "";
  language;
  copied = false;

  ocrResult = "Recognizing...";
  constructor(private formBuilder: FormBuilder) {
    // this.doOCR();

    this.updateprofileimg = this.formBuilder.group({
      img_upload: ["", Validators.required],
    });
  }

  async doOCR(url) {
    const worker = createWorker({
      logger: (m) => console.log(m),
    });
    await worker.load();
    await worker.loadLanguage("eng");
    await worker.initialize("eng");
    const {
      data: { text },
    } = await worker.recognize(url);
    this.ocrResult = text;
    console.log(text);
    await worker.terminate();
  }

  handleUpload(e) {
    this.ocrResult = "Recognizing...";
    this.copied = false;
    if (e.target.files && e.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url = event.target.result;
        // console.log(this.url);
        this.doOCR(this.url);
      };
      reader.readAsDataURL(e.target.files[0]);
    }

    // this.selectedImage = e.target.files[0];
    // let reader = new FileReader();

    // reader.onload = (e: any) => {
    //   this.imageUrl = e.target.result;
    // };
    // reader.readAsDataURL(this.selectedImage);
  }

  changeProfileImage() {
    // const fd = new FormData();
    // fd.append("image", this.selectedImage, this.selectedImage.name);
    console.log("fd");
  }

  copyText() {
    this.copied = true;
    let selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = this.ocrResult;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
  }
}
